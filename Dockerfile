FROM alpine:3 AS builder

ENV S3QL https://github.com/s3ql/s3ql/releases/download/release-3.7.1/s3ql-3.7.1.tar.bz2

RUN apk add \
    build-base \
    fuse3 \
    fuse3-dev \
    linux-headers \
    psmisc \
    python3 \
    python3-dev \
    py3-attrs \
    py3-cryptography \
    py3-defusedxml \
    py3-pip \
    py3-pytest \
    py3-requests \
    py3-setuptools \
    sqlite-dev

RUN pip3 install apsw
RUN pip3 install trio
RUN pip3 install pyfuse3
RUN pip3 install dugong
RUN pip3 install pytest_trio
RUN pip3 install google-auth
# Not really optional?
RUN pip3 install google-auth-oauthlib

# Build
WORKDIR /root
RUN wget "${S3QL}"
RUN tar xvjf s3ql-*.bz2
RUN cd s3ql-* && python3 setup.py build_ext --inplace
# s3ql.logging.QuietError: Unable to parse proxy setting http_proxy=''
RUN cd s3ql-* && unset http_proxy && unset https_proxy && test ! "${TEST}" -gt 0 && python3 -m pytest tests/ || true
# setup.py errors when 'optional' google-auth-oauthlib isn't found
RUN cd s3ql-* && python3 setup.py install

# Cleanup
RUN pip3 uninstall -y pytest_trio
RUN apk del --purge \
    build-base \
    linux-headers \
    python3-dev \
    py3-pip \
    py3-pytest \
    sqlite-dev
RUN rm -rf ~/s3ql-* ~/.cache /var/cache/apk/*

###
# Make a flat image

FROM scratch
COPY --from=builder / /
